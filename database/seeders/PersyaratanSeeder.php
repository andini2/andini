<?php

namespace Database\Seeders;

use App\Models\Master\Periode;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PersyaratanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $faker = \Faker\Factory::create();
        collect([
            [
                'periode_id' => Periode::first()->id ?? '1',
                'nama_persyaratan' => 'Fotokopi KTP',
                'keterangan' => 'Fotokopi KTP yang masih berlaku',
                'is_wajib' => $faker->randomElement(['1', '0']),
            ],
            [
                'periode_id' => Periode::first()->id ?? '1',
                'nama_persyaratan' => 'Fotokopi Akta kelahiran',
                'keterangan' => 'Fotokopi Akta kelahiran yang masih berlaku',
                'is_wajib' => $faker->randomElement(['1', '0']),
            ],
            [
                'periode_id' => Periode::first()->id ?? '1',
                'nama_persyaratan' => 'Surat persetujuan dispensasi nikah dari orang tua',
                'keterangan' => 'Surat persetujuan dispensasi nikah dari orang tua',
                'is_wajib' => $faker->randomElement(['1', '0']),
            ],
            [
                'periode_id' => Periode::first()->id ?? '1',
                'nama_persyaratan' => 'Surat keterangan belum menikah dari kelurahan',
                'keterangan' => 'Surat keterangan belum menikah dari kelurahan',
                'is_wajib' => $faker->randomElement(['1', '0']),
            ],
            [
                'periode_id' => Periode::first()->id ?? '1',
                'nama_persyaratan' => 'Surat keterangan sehat dari dokter',
                'keterangan' => 'Surat keterangan sehat dari dokter',
                'is_wajib' => $faker->randomElement(['1', '0']),
            ]
        ])->each(function ($persyaratan) {
            \App\Models\Master\PersyaratanDispensasi::create($persyaratan);
        });
    }
}
