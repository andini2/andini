<?php

namespace App\Http\Requests\Asesmen;

use Illuminate\Foundation\Http\FormRequest;

class StoreGenerateDispensasiRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        $rules = [
            'nomor_surat' => 'required',
            'file' => 'required|file|mimes:pdf|max:2048',
        ];
        return $rules;
    }

    public function messages(): array
    {
        return [
            'nomor_surat.required' => 'Nomor Surat harus diisi',
            'file.required' => 'File harus diisi',
            'file.file' => 'File harus berupa pdf',
        ];
    }
}
